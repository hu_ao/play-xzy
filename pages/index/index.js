// index.js
const defaultAvatarUrl = 'https://mmbiz.qpic.cn/mmbiz/icTdbqWNOwNRna42FI242Lcia07jQodd2FJGIYQfG0LAJGFxM4FbnQP6yfMxBgJ0F3YRqJCJ1aPAK2dQagdusBZg/0'
const app = getApp();
Component({
  data: {
    motto: '',
    userInfo: {
      avatarUrl: defaultAvatarUrl,
      nickName: ''
    },
    hasUserInfo: false,
  },
  methods: {
    //头像点击事件
    onChooseAvatar(e) {
      const { avatarUrl } = e.detail
      this.setData({
        "userInfo.avatarUrl": avatarUrl
      })
      app.globalData.userInfo.avatarUrl=this.data.userInfo.avatarUrl
    },
    //输入框输入事件
    onInputChange(e) {
      console.log(e);
      const nickName = e.detail.value
      this.setData({
        "userInfo.nickName": nickName
      })
      app.globalData.userInfo.nickName=nickName
    },
    //获取用户信息
    getUserProfile(e) {
      console.log(1111111111111111111);
      wx.getUserProfile({
        desc: '展示用户信息', 
        success: (res) => {
          console.log(res)
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    },
    //登录按钮点击事件
    onLogin(){
      console.log("执行登录");
      if ((defaultAvatarUrl != this.data.userInfo.avatarUrl) && this.data.userInfo.nickName.length > 0) {
       console.log("登录成功"); 
       wx.reLaunch({
        url: '/pages/media/media'
       })
      }
    }
  },
})
