function getRandomColor() {
  const rgb = []
  for (let i = 0; i < 3; ++i) {
    let color = Math.floor(Math.random() * 256).toString(16)
    color = color.length === 1 ? '0' + color : color
    rgb.push(color)
  }
  return '#' + rgb.join('')
}

Page({
  onShareAppMessage() {
    return {
      title: 'video',
      path: 'page/component/pages/video/video'
    }
  },

  onReady() {
    this.videoContext = wx.createVideoContext('myVideo')
  },

  onHide() {

  },

  data: {
    // https://us-xpc5.xpccdn.com/38b7866d-5bba-4370-b29b-d199c70b97eb.mp4
    // https://us-xpc5.xpccdn.com/ad46d711-b28e-4cd9-b4ea-bfb29880ce00.mp4
    inputValue: '',
    currentVedio:'',
    showValue:'',
    videoUrl:'',
    src: '',
    danmuList:
    [{
      text: '欢迎使用自定义视频播放器~~~',
      color: '#ff0000',
      time: 1
    }, {
      text: '赶快加入弹幕大军吧',
      color: '#ff00ff',
      time: 2
    }],
  },

  //更改播放地址
  playVideo(){
    console.log("准备播放视频 =>>>",this.data.videoUrl);
    this.setData({
      currentVedio:this.data.videoUrl
    })
  },

  //弹幕输入事件
  bindInputBlur(e) {
    this.setData({
      inputValue:e.detail.value
    })
  },

  //地址输入事件
  bindInput(e){
    this.setData({
      "videoUrl": e.detail.value
    });
  },

  bindButtonTap() {
    const that = this
    wx.chooseVideo({
      sourceType: ['album', 'camera'],
      maxDuration: 60,
      camera: ['front', 'back'],
      success(res) {
        that.setData({
          src: res.tempFilePath
        })
      }
    })
  },

  //播放按钮事件触发
  bindPlayVideo() {
    this.videoContext.play()
  },

  //发送弹幕
  bindSendDanmu() {
    this.videoContext.sendDanmu({
      text: this.data.inputValue,
      color: getRandomColor()
    })
    
    this.setData({
      inputValue:'',
      showValue:''
    })
  },

  //错误回调
  videoErrorCallback(e) {
    console.log('视频错误信息:')
    console.log(e.detail.errMsg)
  }
})