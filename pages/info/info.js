// pages/info/info.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userAvatar:'',
    nickName:'',
    menuList:[
      {
        name:'看视频',
        to:'/pages/media/media',
      },
      {
        name:'链接放视频',
        to:'/pages/global/global',
      },
      {
        name:'退出',
        to:'/pages/index/index',
      },
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.setData({
      userAvatar:app.globalData.userInfo.avatarUrl,
      nickName:app.globalData.userInfo.nickName
    })
  },

  //点击跳转事件
  turnTo(e){
    console.log(e.currentTarget.dataset.to);
    wx.reLaunch({
      url: e.currentTarget.dataset.to,
    })
  }
})