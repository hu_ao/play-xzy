// 随机文字颜色
function getRandomColor() {
  const rgb = []
  for (let i = 0; i < 3; ++i) {
    let color = Math.floor(Math.random() * 256).toString(16)
    color = color.length === 1 ? '0' + color : color
    rgb.push(color)
  }
  return '#' + rgb.join('')
}

Page({
  onShareAppMessage() {
    return {
      title: 'video',
      path: '/pages/video/video'
    }
  },

  onReady() {
    this.videoContext = wx.createVideoContext('myVideo')
  },


  data: {
    inputValue: '',
    showValue:'',
    currentVedio:'',
    src: '',
    danmuList:
    [{
      text: '快在输入框中弹幕加入弹幕大军吧~',
      color: '#ff0000',
      time: 1
    }, {
      text: '让人满怀期待的视频',
      color: '#ff00ff',
      time: 3
    }],
    videoList:[
      {
        id: "01",
        name:"原神 I 旅人",
        picture:"/static/icon/play.png",
        vedioUrl:"https://us-xpc5.xpccdn.com/57b84bc8-8500-4f8e-9005-b68ce37f4752.mp4"
      },
      {
        id: "02",
        name:"当元宇宙遇到二次元画风",
        picture:"/static/icon/play.png",
        vedioUrl:"https://us-xpc5.xpccdn.com/ad46d711-b28e-4cd9-b4ea-bfb29880ce00.mp4"
      },
      {
        id: "03",
        name:"这首《Monsters》如今治愈了多少人！！",
        picture:"/static/icon/play.png",
        vedioUrl:"https://us-xpc5.xpccdn.com/74a36e05-f4bf-436c-bfc6-d8a599ab819f.mp4"
      },
      {
        id: "04",
        name:"极致舒服，来感受下二次元风景的美吧！！！",
        picture:"/static/icon/play.png",
        vedioUrl:"https://us-xpc5-l2.xpccdn.com/f7069840-3016-4e43-8805-881a9629667a.mp4"
      },
      {
        id: "05",
        name:"《寄明月》舞蹈",
        picture:"/static/icon/play.png",
        vedioUrl:"https://us-xpc5.xpccdn.com/38b7866d-5bba-4370-b29b-d199c70b97eb.mp4"
      },
      {
        id: "06",
        name:"《风月》国风舞蹈",
        picture:"/static/icon/play.png",
        vedioUrl:"https://us-xpc5-l2.xpccdn.com/305114a8-c8a1-4141-9e8b-3741ecdcad7b/4d5c6649-5cfd-4a76-bf1c-034e97a3dce9.mp4"
      },
    ]
  },

  //设置播放地址
  bindInputBlur(e) {
    this.setData({
      inputValue : e.detail.value
    }) 
  },

  
  // bindButtonTap() {
  //   const that = this
  //   wx.chooseVideo({
  //     sourceType: ['album', 'camera'],
  //     maxDuration: 60,
  //     camera: ['front', 'back'],
  //     success(res) {
  //       that.setData({
  //         src: res.tempFilePath
  //       })
  //     }
  //   })
  // },

  //播放事件
  bindPlayVideo() {
    this.videoContext.play()
  },

  //发送弹幕
  bindSendDanmu() {
    const context = wx.createVideoContext('myVideo')
    context.sendDanmu({
      text: this.data.inputValue,
      color: getRandomColor()
    })
    this.setData({
      inputValue: '',
      showValue:''
    })
  },

  //错误回调
  videoErrorCallback(e) {
    console.log('视频错误信息:')
    console.log(e.detail.errMsg)
  },

  //当前播放视频标记
  playVideo(e){
    console.log(e.currentTarget.dataset.item.vedioUrl); 
    this.setData({
      "currentVedio" : e.currentTarget.dataset.item.vedioUrl
    })
    
  }
})